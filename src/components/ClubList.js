import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

const ClubList = () => {
  const [clubs, setClubs] = useState([]);

  const getClubs = async () => {
    const response = await axios.get("http://localhost:5000/clubs");
    setClubs(response.data);
    console.log(response.data);
  };

  useEffect(() => {
    getClubs();
  }, []);

  const deleteClub = async (clubId) => {
    try {
      await axios.delete(`http://localhost:5000/club/${clubId}`);
      getClubs();
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="container my-5">
      <Link className="button is-success mb-5" to="/add">
        Add New Club
      </Link>
      <div className="columns is-multiline">
        {clubs.map((club) => (
          <div className="column is-one-quarter" key={club.id}>
            <div className="card">
              <div className="card-image">
                <figure className="image is-4by3">
                  <img src={club.url} alt="image" />
                </figure>
              </div>
              <div className="card-content">
                <div className="media">
                  <div className="media-content">
                    <p className="title is-4">{club.name}</p>
                  </div>
                </div>
              </div>
              <footer className="card-footer">
                <Link to={`edit/${club.id}`} className="card-footer-item">
                  Edit
                </Link>
                <a
                  onClick={() => deleteClub(club.id)}
                  className="card-footer-item"
                >
                  Delete
                </a>
              </footer>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default ClubList;
