import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import AddClub from "./components/AddClub";
import ClubList from "./components/ClubList";
import EditClub from "./components/EditClub";

const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<ClubList />} />
        <Route path="add" element={<AddClub />} />
        <Route path="edit/:id" element={<EditClub />} />
      </Routes>
    </BrowserRouter>
  );
};

export default App;
